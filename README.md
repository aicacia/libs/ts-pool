# ts-pool

[![license](https://img.shields.io/badge/license-MIT%2FApache--2.0-blue")](LICENSE-MIT)
[![docs](https://img.shields.io/badge/docs-typescript-blue.svg)](https://aicacia.gitlab.io/libs/ts-pool/)
[![npm (scoped)](https://img.shields.io/npm/v/@aicacia/pool)](https://www.npmjs.com/package/@aicacia/pool)
[![pipelines](https://gitlab.com/aicacia/libs/ts-pool/badges/master/pipeline.svg)](https://gitlab.com/aicacia/libs/ts-pool/-/pipelines)

an object pool
